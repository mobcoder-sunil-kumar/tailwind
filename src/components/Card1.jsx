const Card1 = (props) => {
    let { img, alt, title, text} = props;
    return (
        <div class="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4">
        <div class="flex-shrink-0">
          <img class="h-12 w-12" src={img} alt={alt} />
        </div>

        <div>
          <div class="text-xl font-medium text-black">{title}</div>
          <p class="text-gray-500">{text}</p>
        </div>

      </div>
    )
}

export default Card1
