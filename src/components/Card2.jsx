import React from 'react';

const Card2 = (props) => {
  let { img, alt, title, text, button } = props;
  return (
    <div className="m-10 py-8 px-8 max-w-md mx-auto bg-white rounded-xl shadow-md space-y-2 sm:py-4 sm:flex sm:item-center sm:space-y-0 sm:space-x-6">
      <img
        className="block mx-auto h-24 rounded-full sm:mx-0 sm:flex-shrink-0"
        src={img}
        alt={alt}
      />
      <div className="text-center space-y-2 sm:text-left">
        <div className="space-y-0.5">
          <p className="text-lg text-black font-medium">{title}</p>
          <p className="text-gray-500 font-medium">{text}</p>
        </div>
        <button className="px-4 py-1 text-sm text-purple-600 font-semibold rounded-full border border-purple-200 hover:text-white hover:bg-purple-600 hover:border-transparent focus:outline-none focus:ring-purple-600 focus:ring-offset-2">
          {button}
        </button>
      </div>
    </div>
  );
};

export default Card2;
