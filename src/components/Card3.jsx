import React from 'react';

const Card3 = (props) => {
  let { img, alt, heading, title, text } = props;
  return (
    <div className="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-2xl">
      <div className="md:flex">
        <div className="md:flex-shrink-0">
        <img
          className="h-48 w-full object-cover md:h-full md:w-48"
          src={img}
          alt={alt}
        />
        </div>
        <div className="p-8">
        <div className="uppercase tracking-wide text-sm text-indigo-500 font-semibold">{heading}</div>
        <a hre="#" className="block mt-1 text-lg leading-tight font-medium text-black hover:underline">{title}</a>
        <p className="mt-2 text-gray-500 " >{text}</p>
        </div>
      </div>
    </div>
  );
};

export default Card3;

// sm : 640
// md : 768
// lg : 1024
// xl : 1280
// 2xl : 1536

// width of 16 by default, 32 on medium screens, and 48 on large screens
// eslint-disable-next-line no-lone-blocks
{
  /* <div class="w-16 md:w-32 lg:w-48" />  */
}
