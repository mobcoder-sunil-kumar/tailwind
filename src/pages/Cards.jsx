import React from 'react';

import { Card1, Card2, Card3 } from '../components';
import img from '../img/img.jpg';

const Cards = () => {
  return (
    <div>

      <Card1
        img={card1.img}
        alt={card1.alt}
        title={card1.title}
        text={card1.text}
      />
      <Card2
        img={card2.img}
        alt={card2.alt}
        title={card2.title}
        text={card2.text}
        button={card2.button}
      />
      {/* responsive design */}
      <Card3
        img={card3.img}
        alt={card3.alt}
        heading={card3.heading}
        title={card3.title}
        text={card3.text}
      />
    </div>
  );
};

export default Cards;

const card3 = {
  img: img,
  alt: 'Profile pic',
  heading: 'Case study',
  title: 'Finding customers for your new business',
  text: 'Getting a new business off the ground is a lot of hard work. Here are five ideas you can use to find your first customer.',
};
const card2 = {
  img: img,
  alt: 'Profile pic',
  title: 'Sunil Kumar Yadav',
  text: 'Software Engineer',
  button: 'Message',
};

const card1 = {
  img: img,
  alt: 'ChitChat Logo',
  title: 'ChitChat',
  text: 'You have a new message!',
};
