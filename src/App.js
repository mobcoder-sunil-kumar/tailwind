import { useState } from 'react';
import { Cards, Dashboard, Layout } from './pages';

function App() {
  const [option, setOption] = useState(true);
  return (
    <>
      <button onClick={() => setOption(!option)}>Click me!</button>
      {/* {option ? <Cards /> : <Dashboard />} */}
      <Layout />
    </>
  );
}

export default App;
